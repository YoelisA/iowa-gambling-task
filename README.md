

## The Iowa Gambling Task

![http://i.imgur.com/YUuOTv0.png]()

[Click-here to try the demo online]: https://romantic-darwin-25ddf9.netlify.app

The Iowa Gambling Task is a classical task in psychology.

Created originally by Antoine Bechara and Antonio Damasion, this task aims to simulate real-life decision making associated with different rewards and punishments.

This implementation tries to follow the original design with less trials.  

*Disclaimer* : The visual appearance of the cards and the approximate instructions might, however, produce some undesired outcomes.

[Iowa Gambling Task (IGT)]: https://en.wikipedia.org/wiki/Iowa_gambling_task

## Why do I implement this task ?

I wanted to create a small and focus production-ready project show-casing atomic commits,  exhaustive test coverage, sufficient documentation and continuous deployment and integration.

## Where are the tests then ?

I'm not sure what I should test. If you have any idea, please reach me out !

## How to try it on my local computer ?

- ```git clone  https://gitlab.com/YoelisA/implicit-association-task.git```

- ``` npm install -g elm```

- ``` npm install -g create-elm-app```

- ```cd elm-gambling```

- ```elm-app start```

## What are the next steps ?

- [ ] Check the user screen size to display the task only if it has to proprer size.
- [ ] Add the possibility to download the .CSV
- [ ] Add some data persistence.

## How this project is protected ?

This is project is under the MIT license.

> ```
> MIT License
>
> Copyright (c) 2020 Acourt Yoelis
>
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.
> ```
